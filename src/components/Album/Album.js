import React, {Component} from 'react';
import './Album.scss'
import Button from "../Button/Button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import { faStar } from '@fortawesome/free-solid-svg-icons'


class Album extends Component {
    constructor(props) {
        super(props);
        this.state = {
            albums: props.albums,
            starColor:true,
            id:props.id,
            onclick: props.onclick
        }

    }
    changeColorStar=(e)=>{
        this.setState({
            starColor:!this.state.starColor
       });
        if (this.state.starColor){
            e.target.style.color = 'gold'
        }else {
            e.target.style.color = 'grey'
        }



    };


    render() {
        const albums = this.state.albums;
        return (
            <div className='album'>
                {
                    albums.map((card, ind) => {
                        return  <div key={ind} className="card">
                            <img className='album-img' src={card.path} alt="image-1"/>
                            <div className='album-info'>
                                <h6 className='album-name'>{card.name}</h6>
                                <FontAwesomeIcon className='star-icon' onClick={this.changeColorStar} icon={faStar}/>
                                <p className='album-text'>{card.text}</p>
                                <p className='album-price'>{card.price}</p>
                                <Button  id={this.state.id} text='ADD TO CART' onclick={this.state.onclick}/>



                            </div>
                        </div>
                    })
                }

            </div>
        );
    }
}

export default Album;
