import React, {Component} from 'react';
import './AlbumList.scss'
import Album from "../Album/Album";

class AlbumList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allCards: props.albums
        }
    }

    render() {
        return<div className='album'>
            <Album albums={this.state.allCards} />
        </div>;
    }
}

export default AlbumList;
