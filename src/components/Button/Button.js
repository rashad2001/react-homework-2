import React, {Component} from 'react';
import './Button.scss'

class Button extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: this.props.text,
            onclick: this.props.onclick,
            id:this.props.id
        }
    }

    render() {
        return (
            <button className='button'
                    onClick={this.state.onclick}
            id={this.state.id}>
                {this.state.text}</button>
        );
    }
}

export default Button;
