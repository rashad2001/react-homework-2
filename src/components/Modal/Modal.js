import React, {Component} from 'react';
import './Modal.scss'
import icon from  '../img/x.png'
class Modal extends Component {
    constructor(props) {
        super(props);
        this.state={
            header: this.props.header,
            text: this.props.text,
            actions: this.props.actions,
            ind: this.props.ind,
            closeButton: this.props.closeButton
        }
    }


    render() {
        return (
            <div className='modal'>
                <div className='modal-header'>
                    {this.state.header}
                    <img  className='modal-icon' onClick={this.state.closeButton} src={icon} alt="icon"/>
                </div>
                <p className='modal-text'>{this.state.text}</p>
                <p className='modal-actions'>{this.state.actions}</p>
            </div>


        );
    }
}

export default Modal;
