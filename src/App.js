import React, {Component}  from 'react';
import './App.css'
import AlbumList from "./components/AlbumList/AlbumList";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      albums: []

    }
  }


    componentDidMount() {
        fetch('../../../products.json')
            .then(res => res.json())
            .then((data) => {
                this.setState({
                    albums: data
                });
            })
    }


    render() {
        return (
            <div className='App'>
                {
                    this.state.albums.length > 0 ? <AlbumList albums={this.state.albums}/> : null

                }
            </div>
        );
    }
}

export default App;
